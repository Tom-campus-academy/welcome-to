let struct_game_cards = [
    { nb: 3, value: '1', src: "assets/img/1.png" },
    { nb: 3, value: '2', src: "assets/img/2.png" },
    { nb: 3, value: '14', src: "assets/img/14.png" },
    { nb: 3, value: '15', src: "assets/img/15.png" },
    { nb: 4, value: '3', src: "assets/img/3.png" },
    { nb: 4, value: '13', src: "assets/img/13.png" },
    { nb: 5, value: '4', src: "assets/img/4.png" },
    { nb: 5, value: '12', src: "assets/img/12.png" },
    { nb: 6, value: '5', src: "assets/img/5.png" },
    { nb: 6, value: '11', src: "assets/img/11.png" },
    { nb: 7, value: '6', src: "assets/img/6.png" },
    { nb: 7, value: '10', src: "assets/img/10.png" },
    { nb: 8, value: '7', src: "assets/img/7.png" },
    { nb: 8, value: '9', src: "assets/img/9.png" },
    { nb: 9, value: '8', src: "assets/img/8.png" }
];

let game_cards = [];

for(let i = 0; i < struct_game_cards.length; i++)
{

    for (j = 0; j < struct_game_cards[i].nb; j++)
    {
        game_cards.push(struct_game_cards[i]);
    }

}

let struc_house_cards = [
    { nb: 9, value: 'Fabricant de piscine', src: "assets/img/piscine.png"},
    { nb: 9, value: "Agence d'interim", src: "assets/img/agence_interim.png" },
    { nb: 9, value: 'Numero Bis', src: "assets/img/numero_bis.png" },
    { nb: 18, value: 'Paysagiste', src: "assets/img/paysagiste.png" },
    { nb: 18, value: 'Agent immobilier', src: "assets/img/agent_immobilier.png" },
    { nb: 18, value: 'Geometre', src: "assets/img/geometre.png" },
  
];

let house_cards = [];

for(let i = 0; i < struc_house_cards.length; i++)
{

    for (j = 0; j < struc_house_cards[i].nb; j++)
    {
        house_cards.push(struc_house_cards[i]);
    }

}

let house_stack1 = [];
let house_stack2 = [];
let house_stack3 = [];

let action_stack1 = [];
let action_stack2 = [];
let action_stack3 = [];

let action_cards_stacks = [action_stack1, action_stack2, action_stack3];
let house_cards_stacks = [house_stack1, house_stack2, house_stack3];

function shuffleArray(array)
{
    for (let i = array.length -1 ; i > 0; i--)
    {
        const j = Math.floor(Math.random() * (i + 1))
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function dispatch_in_three(array_ref, array1, array2, array3)
{
    for (let i = 0; i < array_ref.length / 3; i++)
    {
        array1[i] = array_ref[i];
        array2[i] = array_ref[i + 1];
        array3[i] = array_ref[i + 2];
    }
}

function pick_cards(array1, array2, array3, array4, array5, array6)
{
    if (house_stack1.length != 0){

        card1_action = array1[0];
        card2_action = array2[0];
        card3_action = array3[0];

        card1_house = array4[0];
        card2_house = array5[0];
        card3_house = array6[0];
        
        for (let i = 0; i < action_cards_stacks.length; i++) 
        {
            action_cards_stacks[i].splice(0, 1);
            house_cards_stacks[i].splice(0, 1);
        }

        console.log("Voici vos cartes action : "),
        console.log(card1_action.value);
        console.log(card2_action.value);
        console.log(card3_action.value);

        console.log("Voici vos cartes maison : "),
        console.log(card1_house.value);
        console.log(card2_house.value);
        console.log(card3_house.value);

        document.getElementById('id1').src = card1_house.src;
        document.getElementById('id2').src = card2_house.src;
        document.getElementById('id3').src = card3_house.src;
        document.getElementById('id4').src = card1_action.src;
        document.getElementById('id5').src = card2_action.src;
        document.getElementById('id6').src = card3_action.src;

    }
    else
    {
        alert("Il n'y a plus de cartes disponibles dans la pioche, appuyer sur F5 pour jouer une autre partie.");
    }
}

shuffleArray(game_cards);
shuffleArray(house_cards);
shuffleArray(game_cards);
shuffleArray(house_cards);
dispatch_in_three(game_cards, action_stack1, action_stack2, action_stack3);
dispatch_in_three(house_cards, house_stack1, house_stack2, house_stack3);
pick_cards(action_stack1, action_stack2, action_stack3, house_stack1, house_stack2, house_stack3);



function Displayhistorical() {
    var div = document.getElementById('historique');
    if (div.style.display != "none") {
        div.style.display = "none";
    } 
    else {
        div.style.display = "block";
    }
}


function choix(x){
    switch (x) {
        case 1:
            console.log(card1_action.value);
            console.log(card1_house.value);

            CreateElementHistorical(card1_action.value, card1_house.value);

        break;
        case 2:
            console.log(card2_action.value);
            console.log(card2_house.value);

            CreateElementHistorical(card2_action.value, card2_house.value);

        break;
        case 3:
            console.log(card3_action.value);
            console.log(card3_house.value);

            CreateElementHistorical(card3_action.value, card3_house.value);

        break;         
      }
}

function CreateElementHistorical(card_action_value, card_house_value) {
    let choice1 = document.createElement("li");
    choice1.id = "choice-historical";
    document.getElementById("choice").appendChild(choice1);
    let text1 = document.createElement("h2");
    text1.id = "text-historical";
    document.getElementById("choice-historical").appendChild(text1);
    document.getElementById("text-historical").innerHTML = card_action_value + " - " + card_house_value;
}